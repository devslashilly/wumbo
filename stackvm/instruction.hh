#pragma once

#include <string>

class Instruction
{
    private:
        int _optCode;
        std::string _name;
        std::string _description;
    public:
        Instruction(int _optCode, std::string _name, std::string description);
        ~Instruction();
        int getOptCode();
        std::string getName();
        std::string getDescription();
};
