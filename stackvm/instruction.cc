#include "instruction.hh"

Instruction::Instruction(int optCode, std::string name, std::string description)
{
    _optCode = optCode;
    _name = name;
    _description = description;
}

Instruction::~Instruction(){}

int Instruction::getOptCode()
{
    return _optCode;
}

std::string Instruction::getName() 
{
    return _name;
}

std::string Instruction::getDescription() 
{
    return _description;
}